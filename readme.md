## Signal Cricket Bot
### A Bot that provides live-cricket updates on the Free/Libre Messenger Signal
This bot utilizes the [signalbot framework](https://github.com/filipre/signalbot/) and [python-espncricinfo](https://github.com/outside-edge/python-espncricinfo) package together to provide live cricket scores from current matches.

### Requirements:
- Docker
- Python (atleast 3.6)
- [signal-cli-rest-api](https://github.com/bbernhard/signal-cli-rest-api)
- [signalbot](https://github.com/filipre/signalbot/)
- [python-espncricinfo](https://github.com/outside-edge/python-espncricinfo)

### Procedure:
- Clone the repository.
- Install docker. (On Debian based GNU/Linux systems, use `sudo apt install docker`)
- Get the `signal-cli-rest-api` image from docker and run it in `NORMAL` mode using:  
```
docker run -p 8080:8080 -v $(pwd)/signal-cli-config:/home/.local/share/signal-cli -e 'MODE=normal' bbernhard/signal-cli-rest-api
```  
- Open `127.0.0.1:8000` on your browser and scan the QR code inorder to link your mobile's signal account for the bot to use your number.
- Stop the docker container and run it in `JSON-RPC` mode using:  
```
docker run -p 8080:8080 -v $(pwd)/signal-cli-config:/home/.local/share/signal-cli -e 'MODE=json-rpc' bbernhard/signal-cli-rest-api
```
- To enroll the bot into a group, add it into that group and obtain the group ID using:  
`curl -X GET 'http://127.0.0.1:8080/v1/groups/+49123456789' | python -m json.tool`
- Open `sources` and fill in the required environment variables.  
Then run `source ./sources` to append them to your environment variables.
- Modify the bot's logic in `bot.py` and add additional functionality to the bot in the `commands` directory.
- Finally, run the bot using `python bot.py`

### Images:  
![](./images/bot.png)  
<img src="./images/mobile.png" width="auto" height="800px">
